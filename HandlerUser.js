class HandlerUser {
  constructor() {
    this.gateway_constants = require("./config");
    this.httpProxy = require("express-http-proxy");
    this.endpoint = "http://localhost:8085";
    this.proxy = this.httpProxy(this.endpoint, {
      limit: "50mb",
      proxyReqOptDecorator: (proxyReqOpts, srcReq) => {
        proxyReqOpts.headers[
          "micro-gateway-auth-x"
        ] = this.gateway_constants.access.user;
        return proxyReqOpts;
      }
    });
    this.routes_private = ["/api/users*"];
  }

  handle(req, res, next) {
    this.proxy(req, res, next);
  }
}

module.exports = new HandlerUser();
