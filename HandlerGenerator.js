let jwt = require("jsonwebtoken");
const config = require("./config.js");

class HandlerGenerator {
  token(req, res) {
    let token = jwt.sign({ signed: "test-auth" }, config.secret, {
      expiresIn: "24h" // expires in 24 hours
    });
    res.json({
      success: true,
      message: "Authentication successful!",
      token: token
    });
  }
}

module.exports = new HandlerGenerator();
