let jwt = require("jsonwebtoken");
const config = require("./config.js");

let check = (secrets, req, res, next) => {
  let token = req.headers["x-access-token"] || req.headers["authorization"]; // Express headers are auto converted to lowercase

  if (
    typeof token !== "undefined" &&
    (token.startsWith("Bearer ") || token.startsWith("bearer "))
  ) {
    // Remove Bearer from string
    token = token.slice(7, token.length);
  }

  if (token) {
    let pass = false;
    let last_decoded = "";

    secrets.forEach(secret => {
      jwt.verify(token, secret, (err, decoded) => {
        if (!err) {
          last_decoded = decoded;
          pass = true;
        }
      });
    });

    if (pass) {
      req.decoded = last_decoded;
      next();
    } else {
      return res.json({
        success: false,
        message: "Token is not valid"
      });
    }
  } else {
    return res.json({
      success: false,
      message: "Auth token is not supplied"
    });
  }
};

let checkToken = (req, res, next) => {
  check([config.secret], req, res, next);
};

module.exports = {
  checkToken: checkToken
};
