const express = require("express");
const helmet = require("helmet");
let bodyParser = require("body-parser");
let config = require("./config");
let middleware = require("./middleware");
let cors = require("cors");
let log4js = require("log4js");
let jsonParser = bodyParser.json();

log4js.configure({
  appenders: { gateway: { type: "file", filename: "system.log" } },
  categories: { default: { appenders: ["gateway"], level: "debug" } }
});
const logger = log4js.getLogger("gateway");
const app = express();
app.use(helmet());
app.use(cors());

let handlers = require("./HandlerGenerator");
app.get("/token", jsonParser, handlers.token);

let HandlerUser = require("./HandlerUser");
app.all(HandlerUser.routes_private, middleware.checkToken, (res, req, next) => {
  HandlerUser.handle(res, req, next);
});

const port = config.port;
app.listen(port, () => console.log(`Gateway app listening on port ${port}!`));
